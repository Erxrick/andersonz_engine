#include "Keyboard.h"
#include "GLWindow.h"

Keyboard::Keyboard ()
    {
    }


Keyboard::~Keyboard ()
    {
    }

void Keyboard::Update (float /*dt*/)
    {
    HandleKeys ();      // must check keys before toggles
    HandleToggles ();
    }

bool Keyboard::AddKey (int vKey)
    {
    if (m_numKeys >= maxKeys)
        {
        printf("Keyboard::AddKey() is Full at %d keys", maxKeys);
        return false;
        }
    m_keys[m_numKeys++]           = vKey;
    m_previousKeyboardState[vKey] = { vKey, KeyIsDown (vKey), false, false };
    return true;
    }

bool Keyboard::AddKeys (const char * keys)
    {
    for (const char* p = keys; *p; ++p)
        {
        if (!AddKey (*p)) return false;
        }
    return true;
    }

bool Keyboard::AddToggle (int vKey, bool * pToggle, bool fireOnPress)
    {
    if (m_numToggles >= maxToggles)
        {
		printf("Keyboard::AddToggle() is Full at %d toggles", maxToggles);
        return false;
        }
    if (!AddKey (vKey)) return false;
    m_toggles[m_numToggles++] = { vKey, pToggle, fireOnPress };
    return true;
    }

bool Keyboard::KeyIsDown (int vKey)
    {
    return (0 != GetAsyncKeyState (vKey));
    }

bool Keyboard::KeyWasPressed (int vKey)
    {
    return m_previousKeyboardState[vKey].justPressed;
    }

bool Keyboard::KeyWasReleased (int vKey)
    {
    return m_previousKeyboardState[vKey].justReleased;
    }

void Keyboard::HandleKeys ()
    {
    for (int j = 0; j < m_numKeys; ++j)
        {
        int vKey     = m_keys[j];
        Key *k       = &m_previousKeyboardState[vKey];
        bool isDown  = KeyIsDown (vKey);
        bool wasDown = m_previousKeyboardState[vKey].wasDownLastFrame;
        if (isDown) k->justPressed  = !wasDown;
        else        k->justReleased =  wasDown;
        m_previousKeyboardState[vKey].wasDownLastFrame = isDown;
        }
    }

void Keyboard::HandleToggles ()
    {
    for (int j = 0; j < m_numToggles; ++j)
        {
        KeyToggle* t    = &m_toggles[j];
        int        vKey = t->vKey;
        Key*       k    = &m_previousKeyboardState[vKey];
        if      (k->justPressed  &&  t->toggleOnPress) t->Toggle ();
        else if (k->justReleased && !t->toggleOnPress) t->Toggle ();
        }
    }
