#include "ShaderProgram.h"

ShaderProgram::ShaderProgram()
{
}

ShaderProgram::~ShaderProgram()
{
}
void ShaderProgram::ListShaders()
{
	for (int i = 0; i < maxShaders; i++)
	{
		std::cout << m_shaders[i].id << std::endl;
		std::cout << m_shaders[i].name << std::endl;
		std::cout << m_shaders[i].type << std::endl;
	}
}
bool ShaderProgram::DeleteShaders()
{
	for (int i = 0; i < maxShaders; ++i)
	{
		if (m_shaders[i].id != 0) 
		{
			std::cout << m_shaders[i].name << " is Being Deleted" << std::endl;
			glDeleteShader(m_shaders[i].id);
			if(GLErrorUtil::TestForError("Deleting the shader Failed")) return false;
			m_shaders[i].id = 0;
			m_shaders[i].type = 0;
			for (int k = 0; k < maxShaderNameLen; k++)
			{
				m_shaders[i].name[k] = 0;
			}
		}
	}
	return true;
}

bool ShaderProgram::DeleteProgram()
{
	if (m_id != 0)
	{
		glDeleteProgram(m_id);
		if(GLErrorUtil::TestForError("Deleting The Program Failed")) return false;
		m_id = 0;
	}
	return true;
}
bool ShaderProgram::Initialize()
{
	this->m_id = glCreateProgram();
	return GLErrorUtil::TestForError("Creating the Program Failed");
}

bool ShaderProgram::Shutdown()
{
	if (DeleteShaders() && DeleteProgram())
	{
		std::cout << "The Shader Program Shutdown Properly" << std::endl;
		return true;
	}
	std::cout << "Failed to delete properly" << std::endl;
	return false;
}

bool ShaderProgram::AddVertexShader(const char * const filename)
{
	return AddShader(filename, GL_VERTEX_SHADER);
}

bool ShaderProgram::AddFragmentShader(const char * const filename)
{
	return AddShader(filename, GL_FRAGMENT_SHADER);
}

bool ShaderProgram::AddShader(const char * const filename, GLenum shaderType)
{
	GLuint shaderId = 0;
	shaderId = glCreateShader(shaderType);
	if (shaderId == 0 || GLErrorUtil::TestForError("Failed to create Shader"))
	{
		printf("glCreateShader failed.");
		return false;
	}
	GLchar* shaderSource = FileUtility::ReadFileIntoString(filename);
	int stringCount = 1;
	glShaderSource(shaderId, stringCount, &shaderSource, 0);

	if (GLErrorUtil::TestForError("Shader source broke") == true)
	{
	return false;
	}
	glCompileShader(shaderId);
	if (GLErrorUtil::TestForError("Shader Compiling Failed") == true) {
		glDeleteShader(shaderId);
		return false;
	};
	if (CheckShaderCompileStatus(shaderId))
	{

		if(SaveShader(filename, shaderType, shaderId)) return true;
	}
	return false;
}

bool ShaderProgram::CheckShaderCompileStatus(GLuint shaderID)
{
	GLint testNum = 0;
	glGetShaderiv(shaderID, GL_COMPILE_STATUS, &testNum);
	if(GLErrorUtil::TestForError("glGetShaderiv(compile status) failed") || testNum != GL_TRUE)
	{
		GLint length = 0;
		glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &length);
		GLErrorUtil::TestForError("glGetShaderiv(log length) failed");
		GLchar* buffer = new GLchar[length];
		ShowShaderLogInfo(shaderID, buffer, length);
		delete[] buffer;
		return false;
	};
	std::cout << "The Shader Compiled Properly." << std::endl;
	return true;
}

bool ShaderProgram::CheckProgramStatus(GLenum pname)
{
	GLint programStatus = 0;
	glGetProgramiv(m_id, pname, &programStatus);
	GLErrorUtil::TestForError("The Program Failed to Retreive Its Status");
	if (programStatus == GL_FALSE)
	{
		GLint length = 0;
		glGetProgramiv(m_id, GL_INFO_LOG_LENGTH, &length);
		GLErrorUtil::TestForError("The Program Failed to Retreive Its Log Length");
		GLchar* buffer = new GLchar[length];
		ShowProgramLogInfo(buffer, length, pname);
		delete[] buffer;
		return false;
	}
	return true;
}

bool ShaderProgram::CheckProgramStatus()
{
	if (CheckProgramStatus(GL_LINK_STATUS))
	{
		std::cout << "Succesfully Linked the Program" << std::endl;
	}	else return false;
	glValidateProgram(m_id);
	if (GLErrorUtil::TestForError("The Program Failed to Validate.")) return false;
	if(CheckProgramStatus(GL_VALIDATE_STATUS))
	{
		std::cout << "Succesfully Validated the Program" << std::endl;
	}
	else return false;

	return true;
}

bool ShaderProgram::LinkAndUseProgram()
{
	glLinkProgram(m_id);
	if(GLErrorUtil::TestForError("Linking the Program to the Shader Failed.") || !CheckProgramStatus()) 
	{
		DeleteShaders();
		DeleteProgram();
		return false;
	}
	glUseProgram(m_id);
	return GLErrorUtil::TestForError("glUseProgram() Failed to set the Current Rendering State");
}

GLint ShaderProgram::GetUniformLocation(const GLchar * name)
{
	GLint rValue = glGetUniformLocation(m_id, name);
	if (GLErrorUtil::TestForError("Retrieving the UniformLocation Failed"))
	{
		std::cout << rValue << " was recieved from " << m_id << ": " << name << std::endl;
	}
	if (rValue == -1) std::cout << "This function returns - 1 if name does not correspond to an active uniform variable in program or if name starts with the reserved prefix gl_." << std::endl;
	return rValue;
}

GLint ShaderProgram::GetAttribLocation(const GLchar * name)
{
	GLint rValue = 0;
	rValue = glGetAttribLocation(m_id, name);
	if (GLErrorUtil::TestForError("Retrieving the AttribLocation Failed"))
	{
		std::cout << rValue << " was recieved from " << m_id << ": " << name << std::endl;
	}
	if (rValue == -1) std::cout << "If the named attribute variable is not an active attribute in the specified program object or if name starts with the reserved prefix gl_, a value of -1 is returned." << std::endl;
	return rValue;
}

void ShaderProgram::ShowShaderLogInfo(GLuint shaderID, char * infoBuffer, GLint bufferLen)
{
	GLsizei lengthofBuffer = 0;
	glGetShaderInfoLog(shaderID, bufferLen, &lengthofBuffer, infoBuffer);
	GLErrorUtil::TestForError("glGetShaderInfoLog() failed.");
	std::cout << infoBuffer << std::endl;
}

void ShaderProgram::ShowProgramLogInfo(char * infoBuffer, GLint bufferLen, GLenum pname)
{
	GLsizei lengthOfBuffer = 0;
	glGetProgramInfoLog(m_id, bufferLen, &lengthOfBuffer, infoBuffer);
	GLErrorUtil::TestForError("glGetProgramInfoLog() failed.");
	std::cout << infoBuffer << std::endl;
}

bool ShaderProgram::SaveShader(const char * const filename, GLenum shaderType, GLuint shaderId)
{
	int i = 0;
	if (m_shaders[i].id != 0) i++;
	m_shaders[i].id = shaderId;
	m_shaders[i].type = shaderType;
	strcpy(m_shaders[i].name, filename);
	glAttachShader(m_id, shaderId);
	return GLErrorUtil::TestForError("Failed to Attach the Shader to the Program");
}

