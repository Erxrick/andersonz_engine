#include "GLWindow.h"
#include <iostream>
#include "GameTime.h"
#include "EngineCore.h"
#pragma once

GLWindow::GLWindow()
{
}


GLWindow::~GLWindow()
{
}

void GLWindow::resizeEvent(QResizeEvent * e)
{
	engine->OnResizeWindow( e);
	QGLWidget::resizeEvent(e);
	glViewport(0, 0, this->width(), this->height());
}


bool GLWindow::Initialize()
{
	GLenum result = glewInit();
	if (result != GLEW_OK)
	{
		printf("Sadness");
	}
	setMouseTracking(true);
	GameTime::Initialize();
	bool connected = connect(&m_timer, SIGNAL(timeout()), this, SLOT(Update()));
	printf("\nWindow ");
	connected ? printf("Created\n") : printf("Failed To Create Window\n");
	return connected;
}

bool GLWindow::Shutdown()
{
	GameTime::Shutdown();
	engine->Shutdown();
	printf("Shutdown called, Goodbye cruel world");
	return true;
}

bool GLWindow::RegisterGame(GameTemplate * game)
{
	if (game)
		m_game = game;

	//TODO Log problems, maybe

	return game;
}

void GLWindow::Update()
{
	EngineCore::dt = GameTime::GetLastFrameTime();
	glViewport(0, 0, this->width(), this->height());
	engine->Update();
	engine->Draw();
	this->repaint();
}

void GLWindow::mouseMoveEvent(QMouseEvent * e)
{
	engine->mouseMoveEvent(e);
}

void GLWindow::Run()
{
	m_game->Init();
	m_timer.start();
}
