#pragma once
#include "GraphicalObject.h"
#include "ExportHeader.h"
#include <vector>
class ENGINE_SHARED Renderer
{
	Renderer();
	~Renderer();
	static Renderer* m_this;
public:
	void Initialize();
	void Draw();

	static Renderer* getRenderer();
	static GraphicalObject*  CreateGraphicalComponent();

	static std::vector<GraphicalObject*> m_graphicalObjects;
};

