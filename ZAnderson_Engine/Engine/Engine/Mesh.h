#pragma once
#include <GL\glew.h>
#include "ColorVertexInfo.h"
#include "Component.h"

class Mesh : Component
{
public:
	GLfloat* m_vertices;
	GLuint m_vertCount;

	GLushort* m_indices;
	GLushort m_indexCount;
	int stride = sizeof(ColorVertexInfo);
	void Update() { return; };
	GLfloat GetVertSizeInBytes()
	{
		return m_vertCount * sizeof(ColorVertexInfo);
	};
	GLfloat GetIndexSizeInBytes()
	{
		return m_indexCount * sizeof(GLushort);
	};
	GLfloat* getVerts()
	{
		return m_vertices;
	};
	GLushort* getIndices()
	{
		return m_indices;
	}
	GLuint getVertCount()
	{
		return m_vertCount;
	}
	GLushort getIndexCount()
	{
		return m_indexCount;
	}
};

