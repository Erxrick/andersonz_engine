#include "ShapeGenerator.h"
#include "matrix.hpp"
#include "ColorVertexInfo.h"
#include "gtx/transform.hpp"
#include "GameObject.h"
#include "Component.h"
#include "Transform.h"
#include "RotationComp.h"
//float globalFloatArray[] =
//{
//	+1.0f, +1.0f,  +1.0f, //X Y Z			//0
//	+1.0f, +1.0f,  +1.0f, //R G B
//
//	+1.0f, -1.0f,  +1.0f, //X Y Z			//1
//	+1.0f, +1.0f,  +1.0f, //R G B
//	
//	-1.0f, -1.0f,  +1.0f, //X Y Z			//2
//	+1.0f, +1.0f,  +1.0f, //R G B
//	
//	-1.0f, -1.0f,  +1.0f, //X Y Z			//3
//	+1.0f, +1.0f,  +1.0f, //R G B
//	
//	-1.0f, +1.0f,  +1.0f, //X Y Z			//4
//	+1.0f, +1.0f,  +1.0f, //R G B
//	
//	+1.0f, +1.0f,  +1.0f, //X Y Z			//5
//	+1.0f, +1.0f,  +1.0f, //R G B
//
//};
ColorVertexInfo cubeVertData[] = {
	//{-1.0f, +1.0f, +1.0f, +1.0f, +1.0f, +1.0f},
	//{ -1.0f, +1.0f, -1.0f, +1.0f, +1.0f, +1.0f },
	//{ +1.0f, +1.0f, +1.0f, +1.0f, +1.0f, +1.0f },
	//{ +1.0f, +1.0f, -1.0f, +1.0f, +1.0f, +1.0f },
	///////////////
	//{ -1.0f, -1.0f, +1.0f, +1.0f, +1.0f, +1.0f },
	//{ -1.0f, -1.0f, -1.0f, +1.0f, +1.0f, +1.0f },
	//{ +1.0f, -1.0f, +1.0f, +1.0f, +1.0f, +1.0f },
	//{ +1.0f, -1.0f, -1.0f, +1.0f, +1.0f, +1.0f }
	{ +1.0f, +1.0f, +1.0f, +0.0f, +0.0f, +1.0f },
	{ +1.0f, +1.0f, -1.0f, +0.0f, +1.0f, +0.0f },
	{ +1.0f, -1.0f, +1.0f, +1.0f, +0.0f, +0.0f },
	{ +1.0f, -1.0f, -1.0f, +1.0f, +1.0f, +1.0f },
	/////////////
	{ -1.0f, +1.0f, +1.0f, +1.0f, +1.0f, +0.0f },
	{ -1.0f, +1.0f, -1.0f, +1.0f, +0.0f, +1.0f },
	{ -1.0f, -1.0f, +1.0f, +0.0f, +1.0f, +1.0f },
	{ -1.0f, -1.0f, -1.0f, +0.0f, +0.0f, +0.0f }
	//{-1.0f, +1.0f, +1.0f, +1.0f, +1.0f, +1.0f},		{+1.0f, +1.0f, +1.0f, +1.0f, +1.0f, +1.0f},		{-1.0f, -1.0f, +1.0f, +1.0f, +1.0f, +1.0f},		{+1.0f, -1.0f, +1.0f, +1.0f, +1.0f, +1.0f},
	////front
	//{-1.0f, +1.0f, -1.0f, +1.0f, +1.0f, +1.0f},		{+1.0f, +1.0f, -1.0f, +1.0f, +1.0f, +1.0f},		{-1.0f, -1.0f, -1.0f, +1.0f, +1.0f, +1.0f},		{+1.0f, -1.0f, -1.0f, +1.0f, +1.0f, +1.0f},
	////back
	//{-1.0f, +1.0f, -1.0f, +1.0f, +1.0f, +1.0f},		{-1.0f, -1.0f, -1.0f, +1.0f, +1.0f, +1.0f},		{-1.0f, -1.0f, +1.0f, +1.0f, +1.0f, +1.0f},		{-1.0f, +1.0f, +1.0f, +1.0f, +1.0f, +1.0f },
	////left
	//{+1.0f, +1.0f, -1.0f, +1.0f, +1.0f, +1.0f},		{+1.0f, -1.0f, -1.0f, +1.0f, +1.0f, +1.0f},		{+1.0f, -1.0f, +1.0f, +1.0f, +1.0f, +1.0f},		{+1.0f, +1.0f, +1.0f, +1.0f, +1.0f, +1.0f},
	////right
	//{-1.0f, +1.0f, +1.0f, +1.0f, +1.0f, +1.0f},		{+1.0f, +1.0f, +1.0f, +1.0f, +1.0f, +1.0f},		{+1.0f, +1.0f, -1.0f, +1.0f, +1.0f, +1.0f},		{-1.0f, +1.0f, -1.0f, +1.0f, +1.0f, +1.0f },
	////top
	//{-1.0f, -1.0f, +1.0f, +1.0f, +1.0f, +1.0f},		{+1.0f, -1.0f, +1.0f, +1.0f, +1.0f, +1.0f},		{+1.0f, -1.0f, -1.0f, +1.0f, +1.0f, +1.0f},		{-1.0f, -1.0f, -1.0f, +1.0f, +1.0f, +1.0f}
	////bottom
};
GLushort indices[] = {
	/*0, 1, 2,
	3, 1, 2,
	//0, 1, 3,
	//0, 3, 2,
	//
	4, 5, 6,
	7, 5, 6,
	//
	1, 3, 5,
	7, 3, 5,
	//
	0, 2, 4,
	6, 2, 4,
	//
	3, 2, 7,
	6, 2, 7,
	//
	1, 0, 5,
	4, 0, 5*/
	0, 1, 3,
	0, 1, 5,
	0, 2, 3,
	0, 2, 6,
	0, 4, 5,
	0, 4, 6,
	1, 3, 5,
	2, 3, 6,
	4, 5, 6,
	3, 5, 7,
	3, 6, 7,
	5, 6, 7
	//0, 1, 2, 2, 3, 1, //front
	//4, 5, 6, 6, 7, 5, //back
	//8, 9, 10, 10, 8, 11,  //left
	//12, 13, 14, 14, 12, 15, //right
	//16, 17, 18, 18, 16, 19, //top
	//20, 21, 22, 22, 20, 23 //bottom
};
Mesh ShapeGenerator:: m_mesh;
 
void ShapeGenerator::Initialize()
{
	m_mesh.m_vertices = &cubeVertData[0].m_vertex.X;
	m_mesh.m_indices = indices;
	m_mesh.m_vertCount = 8;
	m_mesh.m_indexCount = 36;
}
void ShapeGenerator::Shutdown()
{
	//delete m_mesh;
}


void ShapeGenerator::SetColors(float * color0, int numVerts, int stride)
{
	float color = glm::linearRand(0.01f, 1.0f); //for random color %
	for (int i = 0; i < numVerts * stride; i+=stride)
	{
		color0[i] = glm::linearRand(0.01f, 1.0f);
		color0[i+1] = glm::linearRand(0.01f, 1.0f);
		color0[i+2] = glm::linearRand(0.01f, 1.0f);
	}
}

void ShapeGenerator::SetCubeMesh(GraphicalObject * gob)
{
	gob->m_mesh = &m_mesh;
	//gob->gameObject->m_transform->m_mesh = &m_mesh;
	float rand = glm::linearRand(0.01f, .25f);
	gob->gameObject->m_transform->m_scale = glm::scale(glm::vec3(rand, rand, rand));

//	gob->m_scale = glm::mat4(glm::linearRand(0.1f, .6f));
	gob->gameObject->GetComponent<RotationComp>()->m_angles = glm::vec3(glm::linearRand(-1.0f, 1.0f), glm::linearRand(-1.0f, 1.0f), glm::linearRand(-1.0f, 1.0f));
	gob->gameObject->GetComponent<RotationComp>()->m_speed = glm::linearRand(-2.0f, 2.0f);
	gob->gameObject->m_transform->m_translate = glm::translate(glm::vec3(glm::linearRand(-5.0f, 5.0f), glm::linearRand(-5.0f, 5.0f), glm::linearRand(-5.0f, 5.0f)));
	gob->m_color = glm::vec3(glm::linearRand(0.01f, 1.0f), glm::linearRand(0.01f, 1.0f), glm::linearRand(0.01f, 1.0f));

}

void ShapeGenerator::SetCubeMeshNotRandom(GraphicalObject * gob)
{
	//gob->m_mesh = &m_mesh;
	//float rand = 1;
	//gob->m_scale = glm::scale(glm::vec3(.5, .5, .5));

	////	gob->m_scale = glm::mat4(glm::linearRand(0.1f, .6f));
	//gob->m_angles = glm::vec3(1, 1, 1);
	//gob->m_speed =1.0f;
	//gob->m_translate = glm::translate(glm::vec3(0.0f, 0.0f, 0.0f));
	//gob->m_color = glm::vec3(1, 1, 1);

}
