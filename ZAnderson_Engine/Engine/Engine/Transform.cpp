#include "Transform.h"
#include "gtx\transform.hpp"
#include "EngineCore.h"


Transform::Transform()
{
}


Transform::~Transform()
{
}

void Transform::Update()
{

}

glm::mat4 Transform::GetTransform()
{
	m_transform = m_translate* (m_rotate*m_scale);
	return m_transform;
}
