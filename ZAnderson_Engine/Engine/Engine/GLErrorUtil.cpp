#include "GLErrorUtil.h"

//fix this class and make it mine

struct MyError
    {
    GLenum error;
    const char* msg;
    };

MyError errors[] =
    {
            { 0x0500, "Invalid Enum Value" },
            { 0x0501, "Invalid Parameter Value" },
            { 0x0502, "Invalid Operation" },
            { 0x0503, "Stack Overflow" },
            { 0x0504, "Stack Underflow" },
            { 0x0505, "Out of Memory" },
            { 0x0506, "Invalid FrameBuffer Operation" },
            { 0x0507, "Context Lost" },
    };

const int numErrors = sizeof(errors) / sizeof(errors[0]);

GLErrorUtil::GLErrorUtil( )
    { }

GLErrorUtil::~GLErrorUtil( )
    { }

const char* GetError(GLenum errNo)
    {
    int index = errNo - 0x0500;
    if ((index >= 0) && (index < numErrors)) return errors[index].msg;
    for (int j = 0; j < numErrors; ++j)
        {
        if (errNo == errors[j].error) return errors[j].msg;
        }
    return "Unknown GL Error!!!";
    }

bool GLErrorUtil::TestForError(const char* msg)
    {
    GLenum err = glGetError( );
    if (err == GL_NO_ERROR) return false;

    for (; err != GL_NO_ERROR; err = glGetError( ))
        {
        printf("%s OpenGL Error found (%d) : %s\n", msg, err, GetError(err));
        }
    return true;
    }

