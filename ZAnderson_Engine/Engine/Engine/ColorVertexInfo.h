#include <GL\glew.h>
#include "VertexInfo.h"
#include "Vertex.h"
#include "Attribute.h"
#pragma once
struct ColorVertexInfo : VertexInfo{
	GLfloat Stride() {
		return sizeof(m_attribute) + sizeof(m_vertex);
	}
	ColorVertexInfo(GLfloat x, GLfloat y, GLfloat z, GLfloat r, GLfloat g, GLfloat b) {
		m_vertex.X = x;
		m_vertex.Y = y;
		m_vertex.Z = z;
		m_attribute.A = r;
		m_attribute.B = g;
		m_attribute.C = b;
	}
	Vertex m_vertex;
	Attribute m_attribute;

};