#include "Camera.h"
#define GLM_ENABLE_EXPERIMENTAL
#include "gtx\transform.hpp"





void Camera::moveLeft(GLfloat dt)
{
m_position = m_position + ((-m_speed * dt) * m_right);
}

void Camera::moveRight(GLfloat dt)
{
	m_position = m_position + ((m_speed * dt) * m_right);
}

void Camera::moveUp(GLfloat dt)
{
	m_position = m_position + ((m_speed * dt) * m_up);
}

void Camera::moveDown(GLfloat dt)
{
	m_position = m_position + ((-m_speed * dt) * m_up);
}

void Camera::moveForward(GLfloat dt)
{
	m_position = m_position + ((m_speed * dt) * m_viewDirection); //note, these might be backwards
}

void Camera::moveBack(GLfloat dt)
{
	m_position = m_position + ((-m_speed * dt) * m_viewDirection);
}

void Camera::MouseRotate(GLfloat dx, GLfloat dy)
{
	m_yawAngle += dx;
	m_pitchAngle -= dy;
	m_right = glm::vec3(glm::rotate(-dx, m_up) * glm::vec4(m_right, 0.0f));
	glm::vec3 forward = glm::cross(m_up, m_right);
	m_viewDirection = glm::vec3((glm::rotate(m_pitchAngle, m_right)) * glm::vec4(forward, 0.0f));
}

glm::mat4* Camera::GetWorldToViewMatrix()
{
	worldToViewMatrix = glm::lookAt(m_position, m_position + m_viewDirection, glm::cross(m_right, m_viewDirection));
	return &worldToViewMatrix;
}
