#include "Perspective.h"



Perspective::Perspective()
{
}


Perspective::~Perspective()
{
}

bool Perspective::Initialize(GLfloat fovy, GLfloat aspect, GLfloat zNear, GLfloat zFar)
{
	m_fovy = fovy;
	m_aspect.m_aspect = aspect;
	m_zNear = zNear;
	m_zFar = zFar;
	return true;
}
void Perspective::Shutdown()
{

}

void Perspective::SetPerspective(GLfloat fovy, GLfloat aspect, GLfloat zNear, GLfloat zFar)
{
	m_fovy = fovy;
	m_aspect.m_aspect = aspect;
	m_zNear = zNear;
	m_zFar = zFar;
}

void Perspective::SetAspect(GLfloat aspect)
{
	m_aspect.m_aspect = aspect;
}

glm::mat4* Perspective::GetPerspective()
{
	persp = glm::perspective(m_fovy, m_aspect.m_aspect, m_zNear, m_zFar) ;
	return &persp;
}
