#pragma once
#include <vector>
#include "ExportHeader.h"
#include <typeinfo.h>
class Component;
class Transform;

class ENGINE_SHARED GameObject
{
public:
	Transform* m_transform;
	std::vector<Component*> m_components;

	GameObject();
	~GameObject();

	void Update();
	void AddComponent(Component* component);

	template <class T> T* GetComponent() const;
};

template <class T> T* GameObject::GetComponent() const
{
	for (Component* c : m_components)
	{
		if (!c) continue;

		if (typeid(T) == typeid(*c))
		{
			return static_cast<T*> (c);
		}
	}

	return nullptr;
}