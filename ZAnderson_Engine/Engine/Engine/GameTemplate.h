#pragma once


class GameTemplate
{
public:
	virtual void Update(float dt) = 0;
	virtual void Draw() = 0;
	virtual bool Init() = 0;
	virtual void Shutdown() = 0;


};