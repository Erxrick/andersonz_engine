#pragma once
#include "ExportHeader.h"
struct Key
    {
    int vKey;
    bool wasDownLastFrame;
    bool justPressed;
    bool justReleased;
    };

struct KeyToggle
    {
    int vKey;
    bool* pToggle;
    bool  toggleOnPress;

    void Toggle () { *(pToggle) = !*(pToggle); }
    };

class ENGINE_SHARED Keyboard
    {
    public:
        Keyboard ();
        ~Keyboard ();

        void Update (float dt);
        bool AddKey (int vKey);
        bool AddKeys (const char* keys);
        bool AddToggle (int vKey, bool* pToggle, bool fireOnPress = true);
        bool KeyIsDown (int vKey);
        bool KeyWasPressed (int vKey);
        bool KeyWasReleased (int vKey);

    private: // private functions
        void HandleKeys ();
        void HandleToggles ();

    private:
        static const int maxKeys    = 20;
        static const int maxToggles = 6; // note: each toggle consumes a key
        KeyToggle m_toggles[maxToggles]{ 0 };
        int	m_keys[maxKeys]{ 0 };
        Key	m_previousKeyboardState[256];
        int	m_numKeys{ 0 };
        int	m_numToggles{ 0 };
    };

