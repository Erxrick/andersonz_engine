#pragma once
#include "GraphicalObject.h"
#include "ExportHeader.h"
class ENGINE_SHARED CubeManager
{
public:
	CubeManager();
	~CubeManager();
	GraphicalObject obj[6];
	int cubeNum = 6;
	bool Initialize();
	void InitializeCubes();
	void Shutdown();
	void Update(float dt);
	void Draw(glm::mat4* perspectiveMatrix, glm::mat4* camView);
};

