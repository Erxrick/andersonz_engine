#include "GraphicalObject.h"
#include "GLErrorUtil.h"
#include "EngineCore.h"

GraphicalObject::GraphicalObject()
{
}


GraphicalObject::~GraphicalObject()
{
}

void GraphicalObject::Draw()
{
	glm::mat4 temp = (*EngineCore::m_perspective.GetPerspective()) * (*EngineCore::camera->GetComponent<Camera>()->GetWorldToViewMatrix()) * gameObject->m_transform->GetTransform();
	glUniformMatrix4fv(locationOfTransform, 1, false, &(temp[0][0]));
	GLErrorUtil::TestForError("Setting the Transform broke");
	glUniform3f(locationOfTint, m_color[0], m_color[1], m_color[2]);
	GLErrorUtil::TestForError("Setting the tint broke");

	glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_SHORT, 0);
}


