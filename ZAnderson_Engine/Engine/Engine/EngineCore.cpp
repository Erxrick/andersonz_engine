#include "EngineCore.h"
#include <iostream>

Keyboard EngineCore::m_keyboard;
Perspective EngineCore::m_perspective;
std::vector<GameObject*> EngineCore::gameObjects;
GameObject* EngineCore::camera;
Renderer* EngineCore::renderer;
GLfloat EngineCore::dt;

EngineCore::EngineCore()
{

}
EngineCore::~EngineCore()
{
	printf("Closing QApplication!");
}

void EngineCore::Shutdown()
{
	SP.Shutdown();
	m_perspective.Shutdown();
//	CM.Shutdown();

}
bool EngineCore::Initialize(int argc, char** argv)
{
	srand((unsigned)time(0));
	printf("Creating QApplication!");
	this->app = new QApplication(argc, argv);
	this->window = new GLWindow();
	window->engine = this;
	renderer = Renderer::getRenderer();


	m_perspective.Initialize(glm::radians(60.0f), (window->width()/window->height()), 0.01f, 100.0f);
	//set up the camera
	gameObjects.push_back(new GameObject());
	gameObjects[0]->AddComponent(new Camera());
	camera = gameObjects[0];
	camera->GetComponent<Camera>()->MouseRotate(1.63f, 0.0f);
	return true;
}
bool EngineCore::RegisterGame(GameTemplate * game)
{
	if(!window)
		return false;
	return window->RegisterGame(game);
}
int EngineCore::Run()
{

	this->window->show();
	renderer->Initialize();
	window->Run();
	SetupShaders();

	return this->app->exec();
}
void EngineCore::SetupShaders()
{
	SP.Initialize();
	SP.AddShader("C:\\Users\\Zachery\\Desktop\\Neumont Work\\Q4\\Engine Class\\ZAnderson_Engine\\Engine\\Engine\\PassThru.vert.shader", GL_VERTEX_SHADER);
	SP.AddShader("C:\\Users\\Zachery\\Desktop\\Neumont Work\\Q4\\Engine Class\\ZAnderson_Engine\\Engine\\Engine\\ColorShader.Frag.Shader", GL_FRAGMENT_SHADER);
	SP.LinkAndUseProgram();
}
//void EngineCore::SetLocations(CubeManager& CM)
//{
//	for (int i = 0; i < 5; i++)
//	{
//		CM.obj[i].locationOfTransform = SP.GetUniformLocation("transform");
//		CM.obj[i].locationOfTint = SP.GetUniformLocation("tint");
//	}
//}

void EngineCore::Draw()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//CM.Draw(m_perspective.GetPerspective(), camera->GetComponent<Camera>()->GetWorldToViewMatrix());
	renderer->Draw();
}

void EngineCore::Update()
{
	ProcessInput();
//	CM.Update(dt);
	for each (GameObject* var in gameObjects)
	{
		var->Update();
	}

}
void EngineCore::ProcessInput()
{
	m_keyboard.Update(EngineCore::dt);
	if(m_keyboard.KeyIsDown('W')) camera->GetComponent<Camera>()->moveForward(EngineCore::dt);
	if(m_keyboard.KeyIsDown('A')) camera->GetComponent<Camera>()->moveLeft(EngineCore::dt);
	if(m_keyboard.KeyIsDown('S')) camera->GetComponent<Camera>()->moveBack(EngineCore::dt);
	if(m_keyboard.KeyIsDown('D')) camera->GetComponent<Camera>()->moveRight(EngineCore::dt);
	if(m_keyboard.KeyIsDown('R')) camera->GetComponent<Camera>()->moveUp(EngineCore::dt);
	if(m_keyboard.KeyIsDown('F')) camera->GetComponent<Camera>()->moveDown(EngineCore::dt);
}

void EngineCore::mouseMoveEvent(QMouseEvent * e)
{
	static int oldX = 0;
	static int oldY = 0;
	if (e->buttons() & Qt::RightButton)
	{
		GLfloat deltaX = e->x() - oldX;
		GLfloat deltaY = e->y() - oldY;
		camera->GetComponent<Camera>()->MouseRotate(deltaX * 0.01f, deltaY * 0.007f);
	}
	oldX = e->x();
	oldY = e->y();

}

void EngineCore::OnResizeWindow(QResizeEvent * e)
{
	if (window == nullptr) return;
	GLfloat aspect = static_cast<float>(window->width()) / static_cast<float>(window->height());
	m_perspective.SetAspect(aspect);
}

GameObject * EngineCore::CreateGameObject()
{
	GameObject* tempObj = new GameObject();
	gameObjects.push_back(tempObj);
	return tempObj;
}

void EngineCore::SetupKeyboard(char * keys)
{
	m_keyboard.AddKeys(keys);

}
