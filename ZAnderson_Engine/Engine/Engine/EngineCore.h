#include "ExportHeader.h"
#include "Qt\qapplication.h"
#include "GLWindow.h"
#include "ShaderProgram.h"
#include "gtc\random.hpp"
#include "GraphicalObject.h"
#include "CubeManager.h"
#include "GameObject.h"
#include "Keyboard.h"
#include "Perspective.h"
#include <vector>
#include "GameTemplate.h"
#include "Renderer.h"
#include "QtGui\qevent.h"

#pragma once

class ENGINE_SHARED EngineCore
{

public:
	QApplication* app;
	GLWindow* window = nullptr;
//	CubeManager CM;

	static GLfloat dt;

	static Keyboard m_keyboard;
	static Perspective m_perspective;
	static GameObject* camera;
	static Renderer* renderer;

public:
	ShaderProgram SP;
	EngineCore();
	~EngineCore();

	bool Initialize(int argc, char** argv);
	void Shutdown();
	bool RegisterGame(GameTemplate* game);
	int Run();
	void SetupShaders();
//	void SetLocations(CubeManager& CM);
	void Update();
	void ProcessInput();
	void Draw();

	//Utility stuff
	void mouseMoveEvent(QMouseEvent* e);
	void OnResizeWindow(QResizeEvent* e);
	static void SetupKeyboard(char* keys);

	//Game object shizz
	static GameObject* CreateGameObject();
	static std::vector<GameObject*> gameObjects;
	//gameObject[0] should always be the camera!
	
};

