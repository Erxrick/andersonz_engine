#pragma once
#include "GL\glew.h"
class Aspect {


	GLfloat m_lastWidth = 0;
	GLfloat m_lastHeight = 0;

public:
	GLfloat m_aspect;
	Aspect();
	void Update(GLfloat height, GLfloat width);
	GLfloat* GetAspect()
	{
		return &m_aspect;
	}
};

