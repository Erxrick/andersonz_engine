#include "CubeManager.h"
#include "ShapeGenerator.h"
#include "gtx\transform.hpp"
#include "GLErrorUtil.h"

CubeManager::CubeManager()
{
}


CubeManager::~CubeManager()
{
}

bool CubeManager::Initialize()
{
	//b.	In Initialize() it will create the OpenGL vertex buffer 
	//object and index buffer object and initialize them.
//	ShapeGenerator::Initialize();
	InitializeCubes();
	GLuint positionOffest = 0;
	GLuint colorOffset = 3 * sizeof(GLfloat);
	GLuint bufID;
	GLuint indexBufID;
//	glEnable(GL_DEPTH_TEST);
//	glDepthFunc(GL_LEQUAL);
	glClearColor(0.3f, 0.1f, 0.7f, 0.0f);
	glGenBuffers(1, &bufID);
	glBindBuffer(GL_ARRAY_BUFFER, bufID);
	glBufferData(GL_ARRAY_BUFFER, obj[0].m_mesh->GetVertSizeInBytes(), obj[0].m_mesh->m_vertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, obj[0].m_mesh->stride, (void*)positionOffest);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, obj[0].m_mesh->stride, (void*)colorOffset);
	//set indices 
	glGenBuffers(1, &indexBufID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, obj[0].m_mesh->GetIndexSizeInBytes(), obj[0].m_mesh->m_indices, GL_STATIC_DRAW);
	return true;
}
void CubeManager::Update(float dt)
{
	for (int i = 0; i < cubeNum; i++)
	{
	//	obj[i].updatedSpeed = (obj[i].m_speed*dt) + obj[i].updatedSpeed;
	//	if (obj[i].updatedSpeed >(2 * 3.1415)) obj[i].updatedSpeed -= (2 * 3.1415);
	//	obj[i].m_rotate = glm::rotate(obj[i].updatedSpeed, obj[i].m_angles);
	//	obj[i].m_transform = obj[i].m_translate* (obj[i].m_rotate*obj[i].m_scale);
	}
}
void CubeManager::Draw(glm::mat4* perspectiveMatrix, glm::mat4* camView)
{
	//for (int i = 0; i < cubeNum; i++)
	//{
	//	glm::mat4 temp = (*perspectiveMatrix) * (*camView) * obj[i].m_transform;
	//	glUniformMatrix4fv(obj[i].locationOfTransform, 1, false, &(temp[0][0]));
	//	GLErrorUtil::TestForError("Setting the Transform broke");
	//	glUniform3f(obj[i].locationOfTint, obj[i].m_color[0], obj[i].m_color[1], obj[i].m_color[2]);
	//	GLErrorUtil::TestForError("Setting the tint broke");

	//	glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_SHORT, 0);
	//}
}
void CubeManager::InitializeCubes()
{
	//c.	In InitializeCubes() it will call ShapeGenerator::MakeCube() for each cube.
	//d.In InitializeCubes() you will initialize all five cubes, ie, assign scales, translations, etc.

	for (int i = 0; i < cubeNum-1; i++)
	{
		ShapeGenerator::SetCubeMesh(&obj[i]);
	}

	ShapeGenerator::SetCubeMeshNotRandom(&obj[5]);

	//do everything else for the cubes
}

void CubeManager::Shutdown()
{
//delete[] obj;
	ShapeGenerator::Shutdown();
}
