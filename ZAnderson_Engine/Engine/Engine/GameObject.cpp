#include "GameObject.h"
#include "Component.h"
#include "Transform.h"
#include "RotationComp.h"


GameObject::GameObject()
{
	Transform* t = new Transform();
	AddComponent(t);
	RotationComp* r = new RotationComp(); //take out of GO
	AddComponent(r);
	m_transform = t;
}


GameObject::~GameObject()
{
	for (Component* c : m_components)
	{
		delete c;
	}
}

void GameObject::Update()
{
	for (Component* c : m_components)
	{
		c->Update();
	}
}

void GameObject::AddComponent(Component* component)
{
	if (!component) return;

	component->gameObject = this;
	m_components.push_back(component);
}
