#pragma once
#include "GL\glew.h"
#include <QtOpenGL\qglwidget>
#include "Qt\qtimer.h"
#include "GameTemplate.h"
#include "Camera.h"
class EngineCore;

class GLWindow : public QGLWidget
{
	//QT Macro, needed for MOC
	Q_OBJECT

public:
	GLWindow();
	~GLWindow();

protected:
	void initializeGL() { Initialize(); };
	void mouseMoveEvent(QMouseEvent* e);


public:
	bool Initialize();
	bool Shutdown();
	bool RegisterGame(GameTemplate* game);
	void Run();

protected:
	void resizeEvent(QResizeEvent* e);

private slots:
	void Update();

	QTimer m_timer;
	GameTemplate* m_game;
public:
	EngineCore* engine;
};

