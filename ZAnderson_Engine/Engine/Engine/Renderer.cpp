#include "GL\glew.h"
#include "Renderer.h"
#include "ShapeGenerator.h"
#include <iostream>
#include "GLErrorUtil.h"

Renderer* Renderer::m_this;
std::vector<GraphicalObject*> Renderer::m_graphicalObjects;

Renderer::Renderer()
{
}


Renderer::~Renderer()
{
}

Renderer * Renderer::getRenderer()
{
	if (!m_this) m_this = new Renderer();
	return m_this;
}

void Renderer::Initialize()
{
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glClearColor(0.0f, 0.5f, 0.3f, 0.0f);
	//b.	In Initialize() it will create the OpenGL vertex buffer 
	//object and index buffer object and initialize them.
	ShapeGenerator::Initialize();

	GLuint positionOffest = 0;
	GLuint colorOffset = 3 * sizeof(GLfloat);
	GLuint bufID;
	GLuint indexBufID;
	//	glEnable(GL_DEPTH_TEST);
	//	glDepthFunc(GL_LEQUAL);
	glClearColor(0.3f, 0.1f, 0.7f, 0.0f);
	glGenBuffers(1, &bufID);
	glBindBuffer(GL_ARRAY_BUFFER, bufID);
	glBufferData(GL_ARRAY_BUFFER, ShapeGenerator::m_mesh.GetVertSizeInBytes(), ShapeGenerator::m_mesh.m_vertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, ShapeGenerator::m_mesh.stride, (void*)positionOffest);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, ShapeGenerator::m_mesh.stride, (void*)colorOffset);
	//set indices 
	glGenBuffers(1, &indexBufID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, ShapeGenerator::m_mesh.GetIndexSizeInBytes(), ShapeGenerator::m_mesh.m_indices, GL_STATIC_DRAW);
}

void Renderer::Draw()
{
	for each (GraphicalObject* var in m_graphicalObjects)
	{
		var->Draw();
	}
}

GraphicalObject * Renderer::CreateGraphicalComponent()
{
	GraphicalObject* GrObj = new GraphicalObject();
	m_graphicalObjects.push_back(GrObj);
	return GrObj;
}

//void Renderer::AddCube()
//{
//	InitializeCubes();
//	GLuint positionOffest = 0;
//	GLuint colorOffset = 3 * sizeof(GLfloat);
//	GLuint bufID;
//	GLuint indexBufID;
//
//	glGenBuffers(1, &bufID);
//	glBindBuffer(GL_ARRAY_BUFFER, bufID);
//	glBufferData(GL_ARRAY_BUFFER, obj[0].m_mesh->GetVertSizeInBytes(), obj[0].m_mesh->m_vertices, GL_STATIC_DRAW);
//	glEnableVertexAttribArray(0);
//	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, obj[0].m_mesh->stride, (void*)positionOffest);
//	glEnableVertexAttribArray(1);
//	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, obj[0].m_mesh->stride, (void*)colorOffset);
//	//set indices 
//	glGenBuffers(1, &indexBufID);
//	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufID);
//	glBufferData(GL_ELEMENT_ARRAY_BUFFER, obj[0].m_mesh->GetIndexSizeInBytes(), obj[0].m_mesh->m_indices, GL_STATIC_DRAW);
//}
