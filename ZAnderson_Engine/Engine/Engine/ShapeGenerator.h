#pragma once
#include "gtc\random.hpp"
#include "ExportHeader.h"
#include "Mesh.h"
#include "GraphicalObject.h"
class ENGINE_SHARED ShapeGenerator
{
public:
	static void Initialize();
	static void Shutdown();
	//static float* GetCubeVerts(int& numVerts);
	static void SetColors(float*color0, int numVerts, int stride);

	static Mesh m_mesh;

	static void SetCubeMesh(GraphicalObject* gob);
	static void SetCubeMeshNotRandom(GraphicalObject* gob);

};

