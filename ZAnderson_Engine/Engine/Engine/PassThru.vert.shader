#version 430 core

layout(location = 0) in vec3 vPosition;
layout(location = 1) in vec3 vColor;

layout (location = 10) uniform vec3 tint;
layout (location = 11) uniform mat4 transform;

out vec4 color;

void main() 
{
	gl_Position = transform * vec4(vPosition, 1.0f);
	vec3 newColor = vec3((vColor.x + tint.x) / 2, (vColor.y + tint.y) / 2, (vColor.z + tint.z) / 2);
	color = vec4(newColor, 1.0f);
}