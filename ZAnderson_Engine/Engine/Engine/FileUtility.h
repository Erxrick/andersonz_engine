#pragma once
#include <iostream>
#include <fstream>
#include "ExportHeader.h"

namespace FileUtility {
	ENGINE_SHARED char* ReadFileIntoString(const char* const filename);

}
