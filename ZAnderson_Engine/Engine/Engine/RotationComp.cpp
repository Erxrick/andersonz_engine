#include "RotationComp.h"
#include "GameObject.h"
#include "Transform.h"
#include "gtx\transform.hpp"
#include "EngineCore.h"

void RotationComp::Update()
{
	updatedSpeed = (m_speed * EngineCore::dt) + updatedSpeed;
	if (updatedSpeed >(2 * 3.1415)) updatedSpeed -= (2 * 3.1415);
	gameObject->m_transform->m_rotate = glm::rotate(updatedSpeed, m_angles);
	gameObject->m_transform->m_transform = gameObject->m_transform->m_translate* (gameObject->m_transform->m_rotate*gameObject->m_transform->m_scale);
}

RotationComp::RotationComp()
{
}


RotationComp::~RotationComp()
{
}
