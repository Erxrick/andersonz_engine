#pragma once
#include "Mesh.h"
#include "matrix.hpp"
#include "ExportHeader.h"
#include "Component.h"
class ENGINE_SHARED GraphicalObject : public Component
{
public:
	GraphicalObject();
	~GraphicalObject();


	Mesh* m_mesh;

	glm::vec3 m_color;

	GLint locationOfTint = 10;
	GLint locationOfTransform = 11;

	void Update() { return; };
	void Draw();
	};

