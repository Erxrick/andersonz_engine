#include "Aspect.h"



Aspect::Aspect()
{
	m_aspect = m_lastWidth / m_lastHeight;
}
void Aspect::Update(GLfloat height, GLfloat width)
{
	{
		if (m_lastWidth != width || m_lastHeight != height)
		{
			m_lastWidth = width;
			m_lastHeight = height;
			m_aspect = m_lastWidth / m_lastHeight;
		}
	}
}

