#pragma once
#include "GL\glew.h"

#include "Component.h"
#include "matrix.hpp"

class RotationComp : public Component
{
public:

	GLfloat updatedSpeed = 0.0f;
	glm::vec3 m_angles;
	GLfloat m_speed = 0.0f;

	void Update();

	RotationComp();
	~RotationComp();
};

