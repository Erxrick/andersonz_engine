#include "FileUtility.h"
/*
b.	The method returns a ptr to a char array that is allocated on the heap (�new�) and thus care must be taken to ensure that the memory is not leaked. If an error occurs, it logs a detailed error message and returns nullptr.
c.	The method opens the file using ifstream, and checks to make sure it is good().
d.	Research ifstream rdbuf() to get a filebuf ptr.
e.	Use the filebuf ptr to seek from 0 to end, this will allow you to determine the size of the file. Example:
auto size   = pbuf->pubseekoff(0, inStream.end);
long buflen = 1 + static_cast<long> (size);
f.	(k) Log the filename and buffer length as a Debug message
g.	(k) if  buflen <= 0, Log an Error message that the method was unable to seek to the end of file = (%s)
h.	new a buffer of the desired length and use { 0 } to pre-fill it with zeroes.
i.	use pubseekpos() to seek back to the beginning of the file in preparation for reading the file
j.	use sgetn() to read the file into the buffer
k.	close the file
l.	(k) Debug Log the filename and file contents in a nice way
m.	return the buffer
n.	(k) test the method with several files to make sure it works. Try an empty file, a small file, a large file.
*/
char * FileUtility::ReadFileIntoString(const char * const filename)
{
	std::ifstream fileStream;
	fileStream.open(filename, std::ifstream::in);
	std::streambuf *psbuf;
	psbuf = fileStream.rdbuf();
	auto size = psbuf->pubseekoff(0, fileStream.end);
	long fsize = 1 + static_cast<long> (size);
	std::cout << std::endl;
	std::cout << "File Name: " << filename << std::endl;
	std::cout << "Buffer Length: " << fsize  << std::endl;
	char * buf = nullptr;
	if (fsize <= 0)
	{
		printf("Unable to seek to the end of file = (%s)\n\n", filename);
	}	else
	{
		buf = new char[fsize] {0};
		psbuf->pubseekpos(fileStream.beg);
		psbuf->sgetn(buf, (fsize-1));
		buf[fsize - 1] = '\0';
		std::cout << std::endl << buf << std::endl;
	}

		fileStream.close();



		return buf;
}
/*
n.	(k) test the method with several files to make sure it works. Try an empty file, a small file, a large file.
*/
