#pragma once
#include "matrix.hpp"
#include "gtc\matrix_transform.hpp"
#include "GL\glew.h"
#include "Aspect.h"
#include "ExportHeader.h"

class ENGINE_SHARED Perspective
{
public:
	Perspective();
	~Perspective();
	bool Initialize(GLfloat fovy, GLfloat aspect, GLfloat zNear, GLfloat zFar);
	void Shutdown();
	void SetPerspective(GLfloat fovy, GLfloat aspect, GLfloat zNear, GLfloat zFar);
	void SetAspect(GLfloat aspect);
	glm::mat4* GetPerspective();

private:
	Aspect m_aspect;
	GLfloat m_fovy;
	GLfloat m_zNear;
	GLfloat m_zFar;
	glm::mat4 persp;

};

