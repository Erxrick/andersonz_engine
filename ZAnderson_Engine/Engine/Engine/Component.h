#pragma once
#include "GameObject.h"
#include "ExportHeader.h"
class ENGINE_SHARED Component
{
public:
	GameObject* gameObject;

	Component() {};
	~Component() {};

	virtual void Update() = 0;
	template <class T> T* GetComponent() const;
};

template <class T> T* Component::GetComponent() const
{
	return gameObject->GetComponent<T>;
}