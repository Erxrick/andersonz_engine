#pragma once
#include "matrix.hpp"
#include "gtx\transform.hpp"
#include "GL\glew.h"
#include "ExportHeader.h"
#include "Component.h"
#include "Transform.h"


class ENGINE_SHARED Camera : public Component
{
public:

	glm::vec3 m_position = glm::vec3(0.0f, 0.0f, -2.0f);
	glm::vec3 m_up = glm::vec3(0.0f, 1.0, 0.0f);
	glm::vec3 m_viewDirection = glm::vec3(0.0f, 0.0f, 2.0f);
	glm::vec3 m_right = glm::vec3(0.0f, 0.0f, 1.0f);
	GLfloat		m_speed = 0.5f;
	GLfloat m_yawAngle = 0.0f;
	GLfloat m_pitchAngle = 0.0f;
	glm::mat4 worldToViewMatrix;

	void moveLeft(GLfloat dt);
	void moveRight(GLfloat dt);
	void moveUp(GLfloat dt);
	void moveDown(GLfloat dt);
	void moveForward(GLfloat dt);
	void moveBack(GLfloat dt);
	void MouseRotate(GLfloat dx, GLfloat dy);
	glm::mat4* GetWorldToViewMatrix();
	void Update() { return; };

};

