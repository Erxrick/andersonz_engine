#pragma once
#include "GL\glew.h"
#include "matrix.hpp"
#include "ExportHeader.h"
#include "Component.h"

class Transform : public Component
{
public:
	glm::mat4 m_scale;
	glm::mat4 m_rotate;
	glm::mat4 m_translate;
	glm::mat4 m_transform;
	glm::mat4 GetTransform(); //build this



	Transform();
	~Transform();
	void Update();
};

