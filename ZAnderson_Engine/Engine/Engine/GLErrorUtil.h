#ifndef GLErrorUTIL_H_
#define GLErrorUTIL_H_

#include <GL\glew.h>
#include <iostream>
//fix this class and make it mine



class GLErrorUtil
    {
    public:
        GLErrorUtil( );
        ~GLErrorUtil( );

    public:
        static bool TestForError(const char* msg);

        template<typename...Args>
        static bool TestForError (const char* const format, Args... args);

    };

template<typename ...Args>
inline bool GLErrorUtil::TestForError (const char * const format, Args ...args)
    {
    const int bufSize = 400;
    char      buffer[bufSize];
    sprintf_s (buffer, bufSize, format, args...);
    return TestForError (buffer);
    }


#endif // ndef GLErrorUtil_H_

