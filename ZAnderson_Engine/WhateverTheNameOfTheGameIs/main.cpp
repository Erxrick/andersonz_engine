#include "EngineCore.h"
#include "GameyThingy.h"
#include "FileUtility.h"

int main(int argc, char** argv) 
{
	EngineCore engine;
	engine.Initialize(argc, argv);
	GameyThingy game;

	game.engine = &engine;
	engine.RegisterGame(&game);


	int rVal = engine.Run();
	engine.Shutdown();
	return rVal;

}