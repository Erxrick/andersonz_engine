#include "GL\glew.h"
#include "GameyThingy.h"
#include <iostream>
#include "QtGui\qevent.h"
#include "GraphicalObject.h"
#include "Component.h"
#include "Renderer.h"
#pragma once

GameyThingy::GameyThingy()
{
	
}

GameyThingy::~GameyThingy()
{
}

bool GameyThingy::Init()
{
	for (int i = 0; i < 500; i++)
	{
		obj[i] = engine->CreateGameObject();
		obj[i]->AddComponent(Renderer::CreateGraphicalComponent());
		ShapeGenerator::SetCubeMesh(obj[i]->GetComponent<GraphicalObject>());
	}
	EngineCore::SetupKeyboard("WASD");//FileUtility::ReadFileIntoString("InputKeys"));
	return true;
}
void GameyThingy::Shutdown()
{
	delete[] obj;
}


//Empty
void GameyThingy::Update(float dt)
{
}
void GameyThingy::Draw()
{	
}





