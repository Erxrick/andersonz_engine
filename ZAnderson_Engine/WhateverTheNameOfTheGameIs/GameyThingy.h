#pragma once
#include "GameTemplate.h"
#include "GLWindow.h"
#include "ShaderProgram.h"
#include "ShapeGenerator.h"
#include "EngineCore.h"
#include "CubeManager.h"

class GameyThingy :
	public GameTemplate
{
public:
	GameyThingy();
	~GameyThingy();

	EngineCore* engine;

	bool Init();
	void Shutdown();
	void Update(float dt);
	void Draw();
	GameObject* obj[500];
};

